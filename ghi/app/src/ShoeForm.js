import React, { useState, useEffect } from 'react';

function ShoeForm() {
	const [manufacturer, setManufacturer] = useState('');
	const [modelName, setModelName] = useState('');
	const [color, setColor] = useState('');
	const [pictureUrl, setPictureUrl] = useState('');
	const [bins, setbins] = useState([]);
	const [bin, setbin] = useState('');

	const fetchData = async () => {
		const Url = 'http://localhost:8100/api/bins/';
		const response = await fetch(Url);
		if (response.ok) {
			const data = await response.json();
			setbins(data.bins);
		}
	};
	useEffect(() => {
		fetchData();
	}, []);

	const handleSubmit = async (event) => {
		event.preventDefault();
		const shoe = {
			manufacturer_name: manufacturer,
			model_name: modelName,
			color,
			picture_url: pictureUrl,
			bin,
		};
		// console.log(shoe);

		const shoeUrl = `http://localhost:8080/api/shoes/`;
		const fetchConfig = {
			method: 'POST',
			body: JSON.stringify(shoe),
			headers: {
				'Content-Type': 'application/json',
			},
		};
		const response = await fetch(shoeUrl, fetchConfig);
		if (response.ok) {
			const newShoe = await response.json();
			setManufacturer('');
			setModelName('');
			setColor('');
			setPictureUrl('');
			setbin([]);
			window.location.replace('/shoes');
		} else {
			console.log('error');
		}
	};

	const handleChangeManufacturer = (event) => {
		const value = event.target.value;
		setManufacturer(value);
	};

	const handleChangeModelName = (event) => {
		const value = event.target.value;
		setModelName(value);
	};

	const handleChangeColor = (event) => {
		const value = event.target.value;
		setColor(value);
	};

	const handleChangePictureUrl = (event) => {
		const value = event.target.value;
		setPictureUrl(value);
	};

	const handleChangebin = (event) => {
		const value = event.target.value;
		setbin(value);
		console.log(value);
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<form onSubmit={handleSubmit}>
						<div className="mb-3">
							<label htmlFor="manufacturer" className="form-label">
								Manufacturer:
							</label>
							<input
								type="text"
								id="manufacturer"
								value={manufacturer}
								onChange={handleChangeManufacturer}
								className="form-control"
							/>
						</div>
						<div className="mb-3">
							<label htmlFor="modelName" className="form-label">
								Model Name:
							</label>
							<input
								type="text"
								id="modelName"
								value={modelName}
								onChange={handleChangeModelName}
								className="form-control"
							/>
						</div>
						<div className="mb-3">
							<label htmlFor="color" className="form-label">
								Color:
							</label>
							<input
								type="text"
								id="color"
								value={color}
								onChange={handleChangeColor}
								className="form-control"
							/>
						</div>
						<div className="mb-3">
							<label htmlFor="pictureUrl" className="form-label">
								Picture URL:
							</label>
							<input
								type="text"
								id="pictureUrl"
								value={pictureUrl}
								onChange={handleChangePictureUrl}
								className="form-control"
							/>
						</div>
						<div className="mb-3">
							<label htmlFor="bin" className="form-label">
								Bin:
							</label>
							<select
								id="bin"
								value={bin}
								name="bin"
								onChange={handleChangebin}
								className="form-select"
							>
								{bins.map((bin) => {
									return (
										<option key={bin.href} value={bin.id}>
											{bin.id}
										</option>
									);
								})}
							</select>
						</div>
						<button type="submit" className="btn btn-primary">
							Create Shoe
						</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default ShoeForm;
