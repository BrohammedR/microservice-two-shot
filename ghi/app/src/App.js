import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import HatForm from './HatForm';
import HatList from './HatList';

function App(props) {
	return (
		<BrowserRouter>
			<Nav />
			<div className="container">
				<Routes>
					<Route path="/" element={<MainPage />} />
					<Route path="/shoes" element={<ShoeList />} />
					<Route path="/shoes/new" element={<ShoeForm />} />
					<Route path="/hats" element={<HatList />} />
					<Route path="/hats/new" element={<HatForm />} />
				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
