import React, { useState, useEffect } from 'react';

function HatList() {
    const [hats, setHats] = useState([]);

    const handleDelete = async (id) => {
        const hatUrl = `http://localhost:8090/api/hats/${id}/`;
        const fetchConfig = {
            method: 'DELETE',
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            setHats(hats.filter((hat) => hat.id));
        }
        document.location.reload();
    }

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    if (hats.length === 0) {
        return <div>
                <div>
                    There are no hats! Click 'Add Hat' to add hats to your collection.
                </div>
                <a href="/hats/new/">
                        <button className="btn btn-primary">Add Hat</button>
                    </a>
            </div>
    }

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Fabric</th>
                        <th>Style Name</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Location</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map((hat) => {
                        return (
                            <tr key={hat.id}>
                                <td>{hat.fabric}</td>
                                <td>{hat.style_name}</td>
                                <td>{hat.color}</td>
                                <td>
                                    <img src={hat.picture_url} alt="" width="100px" height="100px"/>
                                </td>
                                <td>{hat.location.id}</td>
                                <td>
                                    <button onClick={() => handleDelete(hat.id)} type="button" className="btn btn-outline-danger">Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <a href="/hats/new/">
                <button className="btn btn-primary">Add Hat</button>
            </a>
        </>
    )
}

export default HatList;