import React, { useState, useEffect } from 'react';

function ShoeList() {
	const [shoes, setShoes] = useState([]);

	const fetchData = async () => {
		const fetchurl = `http://localhost:8080/api/shoes/`;
		const response = await fetch(fetchurl);
		if (response.ok) {
			const data = await response.json();
			setShoes(data.shoes);
		}
	};

	const handleDelete = async (id) => {
		const shoeUrl = `http://localhost:8080/api/shoes/${id}/`;
		const fetchConfig = {
			method: 'delete',
		};
		const response = await fetch(shoeUrl, fetchConfig);
		if (response.ok) {
			setShoes(shoes.filter((shoe) => shoe.id));
		}

		document.location.reload();
	};

	useEffect(() => {
		fetchData();
	}, []);

	if (!shoes.length) {
		return <div>Go buy some kicks, theres no shoes here 😥</div>;
	}

	return (
		<>
			<a href="/shoes/new/">
				<button>Add Shoe</button>
			</a>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Manufacturer</th>
						<th>Model Name</th>
						<th>Color</th>
						<th>Picture</th>
						<th>Bin</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{shoes.map((shoe) => {
						return (
							<tr key={shoe.id}>
								<td>{shoe.manufacturer_name}</td>
								<td>{shoe.model_name}</td>
								<td>{shoe.color}</td>
								<td>
									<img
										src={shoe.picture_url}
										alt=""
										width="100px"
										height="100px"
									/>
								</td>
								<td>{shoe.bin.id}</td>
								<td>
									<button
										onClick={() => handleDelete(shoe.id)}
										type="button"
										className="btn btn-outline-danger"
									>
										Delete
									</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</>
	);
}

export default ShoeList;
