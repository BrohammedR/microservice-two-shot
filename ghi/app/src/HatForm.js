import React, { useState, useEffect } from 'react';

function HatForm() {
    const [fabric, setFabric] = useState('');
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const [styleName, setStyleName] = useState('');
    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }
    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const [pictureUrl, setPictureUrl] = useState('');
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();

        const hat = {
            fabric: fabric,
            style_name: styleName,
            color: color,
            picture_url: pictureUrl,
            location: location,
        }
    
        const hatUrl = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(hat),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setFabric('');
            setStyleName('');
            setColor('');
            setPictureUrl('');
            setLocation([]);
            window.location.replace('/hats');
        } else {
            console.log('error')
        }
    }
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/'
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                            <label htmlFor="name">Fabric:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStyleNameChange} placeholder="Style name" required type="text" name="style_name" id="style_name" className="form-control"/>
                            <label htmlFor="name">Style Name:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="name">Color:</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} placeholder="Picture url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                            <label htmlFor="name">Picture URL:</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} required id="location" name="location" className="form-select">
                            <option value="">Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.href} value={location.id}>
                                        {location.id}
                                    </option>
                                )
                            })}
                            </select>
                        </div>
                          <button className="btn btn-primary">Create Hat</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HatForm