# Wardrobify

Team:

- Mo - Shoes microservice
- Raymond Quach - Hats Microservice

## Design

## Shoes microservice

The shoe application allows users to manage and keep track of their shoe collection. The application's front end includes a list of shoes as well as forms for adding additional shoes. The application also includes forms for adding a shoe manufacturer, model name, color, picture URL, and bin number, as well as a delete feature to remove the item.

## Hats microservice

Hat application was created to let users create, delete, and/or view their hat collection. The models that were created allows the user to enter the fabric information, style name, color, picture url and location. 
