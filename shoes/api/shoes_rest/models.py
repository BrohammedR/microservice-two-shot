from django.db import models

# Create your models here.

class BinVO(models.Model):
    import_href= models.CharField(max_length=100, null=True)
    bin_number= models.PositiveSmallIntegerField(null=True)
    bin_size= models.PositiveSmallIntegerField(null=True)
    closet_name= models.CharField(max_length=100)

class Shoe(models.Model):
    manufacturer_name= models.CharField(max_length=100)
    model_name= models.CharField(max_length=100)
    color= models.CharField(max_length=50)
    picture_url= models.URLField(max_length=200)

    bin= models.ForeignKey(
        BinVO,
        related_name="shoe",
        on_delete=models.CASCADE,
    )
