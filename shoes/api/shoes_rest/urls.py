from django.urls import path
from .views import api_list_shoes, api_shoe_details


urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:id>/", api_shoe_details, name="api_shoe_details"),
]
