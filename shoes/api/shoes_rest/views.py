from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import BinVO, Shoe
# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model= BinVO
    properties =["bin_number", "closet_name", "bin_size", "import_href", "id"]

class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties=[
        "manufacturer_name",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "id"
    ]
    encoders={"bin":BinVODetailEncoder()}

@require_http_methods(["GET","POST"])
def api_list_shoes(request):
    if request.method=="GET":
        shoe = Shoe.objects.all()
        return JsonResponse(
            {"shoes":shoe},
            encoder=ShoeEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = f'/api/bins/{content["bin"]}/'
            bin = BinVO.objects.get(import_href=bin_href)
            content['bin']=bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Wrong bin number bro, try again"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )

@require_http_methods(['DELETE'])
def api_shoe_details(request,id):
    try:
        shoe= Shoe.objects.get(id=id)
        shoe.delete()
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )
    except Shoe.DoesNotExist:
        return JsonResponse({'message':"invalid shoe, doesnt exist. try again broski"})
